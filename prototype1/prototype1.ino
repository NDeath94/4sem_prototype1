/*
  Tone sketch

  Plays tones through a speaker on digital pin 9
  frequency determined by values read from analog port
*/

//The speaker on pin 9
const int speaker = 9;

//Presssure Pins in the 5 pillows
const int pressureSensor0 = A0;
const int pressureSensor1 = A1;
const int pressureSensor2 = A2;
const int pressureSensor3 = A3;
const int pressureSensor4 = A4;

const int THRESHOLD = 400;


//The force that is put on the individual pressurePins
int force0;
int force1;
int force2;
int force3;
int force4;

void setup()
{
  Serial.begin(9600);
}
void loop()
{
  int activeSensors = 0;
  force0 = analogRead(pressureSensor0);
  force1 = analogRead(pressureSensor1);
  force2 = analogRead(pressureSensor2);
  force3 = analogRead(pressureSensor3);
  force4 = analogRead(pressureSensor4);
  int sensorForces[5] = {force0, force1, force2, force3, force4};
  for (int i; i < 5; i++) {
    if (sensorForces[i] >= 200) {
      activeSensors += 1;
    }
  }
  Serial.print(activeSensors);
  if (activeSensors <= 1) {
    if (sensorForces[0] >= THRESHOLD) {
      playTone(100);
    }

    if (sensorForces[1] >= THRESHOLD) {
      playTone(200);
    }

    if (sensorForces[2] >= THRESHOLD) {
      playTone(300);
    }

    if (sensorForces[3] >= THRESHOLD) {
      playTone(400);
    }

    if (sensorForces[4] >= THRESHOLD) {
      playTone(500);
    }
    else {
      noTone(speaker);
    }
  }
  else if (activeSensors == 2) {
    playTone(100);
    playTone(200);
  }
  else if (activeSensors == 3) {
    playTone(100);
    playTone(200);
    playTone(300);
  }
  else if (activeSensors == 4) {
    playTone(100);
    playTone(200);
    playTone(300);
    playTone(400);
  }
  else if (activeSensors == 5) {
    playTone(100);
    playTone(200);
    playTone(300);
    playTone(400);
    playTone(500);
  }
}

void playTone(int frequency) {
  int duration = 200;
  tone(speaker, frequency, duration); // play the tone
  delay(250); // pause one second
}
